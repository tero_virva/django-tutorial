import datetime

from django.utils import timezone
from django.test import TestCase
from django.core.urlresolvers import reverse

from .models import Question

#index tests
def create_question(question_text, days):
    time = timzone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text, pub_date=time)

class QuestionViewTest(TestCase):
    def test_index_view_with_no_questions(self):
        #if no question, an appropriate message should be displayed
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response, 'No polls are available.')

self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_index_view_with_a_past_question(self):
        # question with a pub_date in the past should be displayed on the index page.
        create_question(question_test="Past question.", days=-30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['latest_question_llist'],['<Question: Past question.>'])
        
    def test_index_view_with_a_future_question(self):
        #future pub_date questions should not be seen in index
        create_question(question_text="Future question.", days=30)
        response = self.client.get(reverse('polls:index'))
        self.assertContains(response, "No polls are available.", status_code=200)
        
self.assertQuerysetEqual(respone.context['latest_question_lust'], [])

    def test_index_view_with_future_question_and_past_question(self):
        # if future and past questions exist, only past should be displayed
        create_question(question_Test="Past question.", days=-30)
        create_question(question_text="Future question.", days =30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['latest_question_list'],['<Question: Past question.>'])
        
    def test_index_view_with_two_past_questions(self):
        # for multiple past questions that are approved to display
        create_question(question_text="Past question 1", days=-30)
        create_question(question_text='Past question 2', days=-5)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['latest_question_list'],['<Question: Past question 2.>', '<Question: Past question1.>'])
        
        
        
#detail tests        
class QuestionIndexDetailTest(TestCase):
    def test_detail_view_with_future_question(self):
        #detail view should returt 404 if question pub_date is in the future
        future_question = create_question(question_text='Future question.', days=5)
        response = self.client.get(reverse('polls:detail', args=(future_question.id,)))
        self.assertEqual(response.status_code, 404)
        
    def test_detail_view_with_a_past_question(self):
        #detail view should show question_text of past pub_date question
        past_question = create_question(question_test='Past question', days=-5)
        response = self.client.get(reverse('polls:detail', args=(past_question.id)))
        self.assertContains(response, past_question.question_text, status_code=200)

        
        
#date tests     
class QuestionMethodTests(TestCase):

    def test_was_published_recently_with_future_question(self):
        """
        was_published_recently() should return False for questions whose
        pub_date is in the future.
        """
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)
        self.assertEqual(future_question.was_published_recently(), False)
    
    def test_was_published_recently_with_old_question(self):
        # was_published_recently() should return False for questions older than 1 day
        time = timezone.now() - datetime.timedelta(days=30)
        old_question = Question(pub_date=time)
        self.assertEqual(old_question.was_published_recently(), False)
    
    def test_was_published_recently_with_recent_question(self):
        # was_published_recently() should return True for questions that are less than one day old
        time = timezone.now() - datetime.timedelta(hours=1)
        recent_question = Question(pub_date=time)
        self.assertEqual(recent_question.was_published_recently(), True)